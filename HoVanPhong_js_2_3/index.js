// Bai_1

function Salary() {
    var soNgay = document.getElementById("day").value * 1;
    var tienNgay = document.getElementById("1_ngay").value * 1;
    var result = soNgay * tienNgay;
    var result=Intl.NumberFormat('vn-VN').format(result)
    document.getElementById("tienLuong").innerText = `tiền lương là: ${result} vnd`;
}

// Bai_2
function soTrungBinh() {
    var soThuNhat = document.getElementById("so_1").value * 1;
    var soThuHai = document.getElementById("so_2").value * 1;
    var soThuBa = document.getElementById("so_3").value * 1;
    var soThuBon = document.getElementById("so_4").value * 1;
    var soThuNam = document.getElementById("so_5").value * 1;
    var result = (soThuNhat + soThuHai + soThuBa + soThuBon + soThuNam) / 5;
    document.getElementById("trungBinh").innerHTML = `Giá trị trung bình là: ${result}`
}

// Bai_3
function tienVND() {
    var tongTien = document.getElementById("tongTien").value * 1;
    var result = 23500 * tongTien;
    var result=Intl.NumberFormat('vn-VN').format(result)
    document.getElementById("vnd").innerHTML = `Số tiền quy đổi là: ${result} vnd`
}
// Bai_4
function chuvi_dientich() {
    var width = document.getElementById("dai").value * 1;
    var height = document.getElementById("rong").value * 1;
    var result_dientich = width * height;
    var result_chuvi = (width + height) * 2;
    document.getElementById("dientich").innerHTML = `diện tích là: ${result_dientich}`;
    document.getElementById("chuvi").innerHTML = `chu vi là : ${result_chuvi}`;
}
// Bai_5
function tinhKiSo() {
    var number = document.getElementById("so").value * 1;
    var result = number % 10 + Math.floor(number / 10);
    document.getElementById("tongKiSo").innerHTML = `Tổng kí số là: ${result}`
}